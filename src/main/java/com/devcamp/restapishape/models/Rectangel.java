package com.devcamp.restapishape.models;

public class Rectangel extends Shape {
  double width = 1.0;
  double length = 1.0;

  public Rectangel() {
    super();
  }

  public Rectangel(double width, double length) {
    this.width = width;
    this.length = length;
  }

  public Rectangel(String color, boolean filled, double width, double length) {
    super(color, filled);
    this.width = width;
    this.length = length;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public double getArea() {
    return width * length;
  }

  public double getPerimeter() {
    return 2 * (width + length);
  }

  @Override
  public String toString() {
    return "Rectangel [Shape[color=" + color + " filled=" + filled + "],width=" + width + ", length=" + length + "]";
  }

}
