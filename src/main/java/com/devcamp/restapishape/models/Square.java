package com.devcamp.restapishape.models;

public class Square extends Rectangel {
  double side;

  public Square() {

  }

  public Square(double side) {
    this.side = side;
  }

  public Square(String color, boolean filled, double side) {
    this.color = color;
    this.filled = filled;
    this.side = side;
    this.width = side;
    this.length = side;
  }

  public double getSide() {
    return side;
  }

  public void setSide(double side) {
    this.side = side;
  }

  public void setWidth(double side) {
    this.width = side;
  }

  public void setLength(double side) {
    this.length = side;
  }

  @Override
  public String toString() {
    return "Square[Rectangle[color=" + color + ", filled=" + filled + "],length=" + length
        + ", width=" + width + "]";
  }

}
