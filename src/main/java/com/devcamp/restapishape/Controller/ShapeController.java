package com.devcamp.restapishape.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapishape.models.Circle;
import com.devcamp.restapishape.models.Rectangel;
import com.devcamp.restapishape.models.Square;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ShapeController {

  @GetMapping("/circle-area")
  public double getCircleArea() {
    Circle circle = new Circle(3.0);
    return circle.getArea();
  }

  @GetMapping("/circle-perimeter")
  public double getCirclePerimeter() {
    Circle circle = new Circle(3.0);
    return circle.getPerimeter();
  }

  @GetMapping("/rectangle-area")
  public double getRectangleArea() {
    Rectangel rectangel = new Rectangel(5.0, 6.0);
    return rectangel.getArea();
  }

  @GetMapping("/rectangle-perimeter")
  public double getRectanglePerimeter() {
    Rectangel rectangel = new Rectangel(5.0, 6.0);
    return rectangel.getPerimeter();
  }

  @GetMapping("/square-area")
  public double getSquareArea() {
    Square square = new Square(7);
    square.setWidth(7.0);
    square.setLength(7.0);
    return square.getArea();
  }

  @GetMapping("/square-perimeter")
  public double getSquarePerimeter() {
    Square square = new Square(7);
    square.setWidth(7.0);
    square.setLength(7.0);
    return square.getPerimeter();
  }
}
